/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "qgpsdevice.h"

#include <qextserialport.h>
#include <QDebug>
#include <QStringList>

#include <math.h>

/**
 * QGPSDevice::QGPSDevice()
 *
 * Takes in an optional serialPort string and sets the serialPort property
 * accordingly.
 *
 * @param char serialPort   Serial port to listen to for GPS dat
 */

QGPSDevice::QGPSDevice(const QString serial_port, const quint32 p_BaudRate) :
  m_Port(0), m_SerialPort(serial_port), m_BaudRate(p_BaudRate)
{
  setDate(QDate());
  setUtcHour(0);
  setUtcMinute(0);
  setUtcSecond(0);
  setLatitude(0);
  setLongitude(0);
  setAltitude(0);
  setHeading(0);
  setSpeed(0);
  setVariation(0);

  connect(this, SIGNAL(NMEAMessageReceived(QString)), SLOT(parseMessage(QString)));
}

QString QGPSDevice::nmeaChecksum(const QString& p_Msg)
{
  return QGPSDevice::nmeaChecksum(p_Msg.toAscii());
}

QString QGPSDevice::utcTime() const
{
  return QString("%1:%2:%3")
          .arg(cur_utcHour, 2, 10, QChar('0'))
          .arg(cur_utcMinute, 2, 10, QChar('0'))
          .arg(cur_utcSecond, 2, 10, QChar('0'));
}

QString QGPSDevice::nmeaChecksum(const QByteArray& p_Msg)
{
  quint8 checksum = 0;
  for (int i = 0; i < p_Msg.size(); i++) {
    checksum = checksum ^ p_Msg.at(i);
  }
  return QString("%1").arg(checksum, 2, 16, QChar('0'));
}

/**
 * QGPSDevice::openDevice()
 *
 * Opens the serial port and sets the parameters for data transfer: parity,
 * stop bits, blocking, etc.
 */

bool QGPSDevice::openDevice()
{
  PortSettings settings = {(BaudRateType)m_BaudRate, (DataBitsType)8, PAR_NONE, STOP_1, FLOW_OFF, 100};
  m_Port = new QextSerialPort(serialPort(), settings, QextSerialPort::EventDriven);

  connect(m_Port, SIGNAL(readyRead()), SLOT(onDataAvailable()));

  if (m_Port->open(QIODevice::ReadWrite | QIODevice::Unbuffered)) {
    qDebug() << "Serial port open (" << (quint32)settings.BaudRate << ")";
    return true;
  } else {
    qWarning() << "Open serial port failed";
    return false;
  }
}

/**
 * QGPSDevice::closeDevice()
 *
 * Closes the serial port
 */
void QGPSDevice::closeDevice()
{
  m_Port->close();
  disconnect(m_Port);
}

/**
 * QGPSDevice::startDevice()
 *
 * Calls start() to begin thread execution
 */
void QGPSDevice::startDevice()
{
  openDevice();
}

void QGPSDevice::stopDevice()
{
  closeDevice();
}

void QGPSDevice::restartDevice()
{
  if (isRunning()) {
    stopDevice();
    startDevice();
  }
}

bool QGPSDevice::isRunning() const
{
  if (m_Port) {
    return m_Port->isOpen();
  } else {
    return false;
  }
}

/**
 * Accessor functions
 */

int QGPSDevice::latDegrees() const    { return (int) (latitude() / 100);                                                  }
int QGPSDevice::latMinutes() const    { return (int) (floor(latitude() - latDegrees() * 100));                            }
int QGPSDevice::latSeconds() const    { return (int) (((latitude() - latDegrees() * 100) - latMinutes()) * 60);           }
int QGPSDevice::longDegrees() const   { return (int) (longitude() / 100);                                                 }
int QGPSDevice::longMinutes() const   { return (int) (floor(longitude() - longDegrees() * 100));                          }
int QGPSDevice::longSeconds() const   { return (int) (((longitude() - longDegrees() * 100) - longMinutes()) * 60);        }

void QGPSDevice::onDataAvailable()
{
  static eNMEAState_t nmeaState = eSerIdle;
  static QByteArray nmeaMsg;

  QByteArray data = m_Port->readAll();
  int idx = 0;
  while (idx < data.size()) {
    switch (nmeaState) {
    case eSerIdle:
       if (data.at(idx) == '$') {
        nmeaState = eSerCRLF;
        nmeaMsg.clear();
       }
       break;

    case eSerCRLF:
      if (data.at(idx) == '\r' || data.at(idx) == '\n') {
        emit NMEAMessageReceived(QString(nmeaMsg));
        nmeaState = eSerIdle;
      } else {
        nmeaMsg.append(data.at(idx));
      }
      break;
    }
    idx++;
  }
}

void QGPSDevice::sendNMEAMessage(QByteArray p_Msg)
{
  QString chksm = QGPSDevice::nmeaChecksum(p_Msg);
  p_Msg.prepend('$');
  p_Msg.append('*');
  p_Msg.append(chksm);
  p_Msg.append("\r\n");
  qDebug() << "Output: " << p_Msg;
  m_Port->write(p_Msg);
}


void QGPSDevice::parseMessage(const QString p_Message)
{
  if (p_Message.startsWith("GPGSV")) {
    if (parseGSV(p_Message)) {
      emit updateSV();
    }
    // bypass updatePosition
    return;
  } else if (p_Message.startsWith("GPGGA")) {
    parseGGA(p_Message);
  } else if (p_Message.startsWith("GPGSA")) {
    parseGSA(p_Message.toStdString().c_str());
  } else if (p_Message.startsWith("GPRMC")) {
    parseRMC(p_Message);
  } else {
    return;
  }
  emit updatePosition();
}

/**
 * QGPSDevice::parseGGA()
 *
 * Parses a GPGGA string that contains fix information, such as
 * latitude, longitude, etc.
 *
 * The format of the GPGGA String is as follows:
 *
 *  $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
 *  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 *  01234567890123456789012345678901234567890123456789012345678901234
 *  |         |         |         |         |         |         |
 *  0         10        20        30        40        50        60
 *
 *  GPGGA       -   Global Positioning System Fix Data
 *  123519      -   Fix taken at 12:35:19 UTC
 *  4807.038,N  -   Latitude 48 deg 07.038' N
 *  01131.000,E -   Longitude 11 deg 31.000' E
 *  1           -   Fix quality:
 *                      0 = Invalid
 *                      1 = GPS fix (SPS)
 *                      2 = DGPS fix
 *                      3 = PPS fix
 *                      4 = Real time kinematic
 *                      5 = Float RTK
 *                      6 = Estimated (dead reckoning)
 *                      7 = Manual input mode
 *                      8 = Simulation mode
 *  08          -   Number of satellites being tracked
 *  0.9         -   Horizontal dissolution of precision
 *  545.4,M     -   Altitude (meters) above sea level
 *  46.9,M      -   Height of geoid (sea level) above WGS84 ellipsoid
 *  (empty)     -   Seconds since last DGPS update
 *  (empty)     -   DGPS station ID number
 *  *47         -   Checksum, begins with *
 *
 * @param char ggaString    The full NMEA GPGGA string, starting with
 *                          the $ and ending with the checksum
 */
void QGPSDevice::parseGGA(const QString& p_gga)
{
  QStringList sl = p_gga.split(QChar(','));

  Q_ASSERT(sl[0] == "GPGGA");

  // [1] => Fix taken at hhmmss.sss UTC
  QString utc = sl[1];
  setUtcHour(utc.left(2).toInt());
  setUtcMinute(utc.mid(2, 2).toInt());
  setUtcSecond(utc.mid(4, 2).toInt());

  // [2] => Latitude ddmm.mmmm; [3] => N or S
  parseLatitude(sl[2], sl[3]);

  // [4] => Longitude ddmm.mmmm; [5] => E or W
  parseLongitude(sl[4], sl[5]);

  // [6] => fix quality
  setFixQuality(sl[6].toInt());
  // [7] => numbers of satellites
  setNumSatellites(sl[7].toInt());
  // [8] => Horizontal Dilution of Precision
  if (!sl[8].isEmpty()) {
    setDillution(sl[8].toFloat());
  }
  // [9] => MSL Altitude
  if (!sl[9].isEmpty()) {
    setAltitude(sl[9].toFloat());
  }
}

/**
 * QGPSDevice::parseGSA()
 *
 * Parses a GPGSA string that contains information about the nature
 * of the fix, such as DOP (dillution of precision) and active satellites
 * based on the viewing mask and almanac data of the reciever.
 *
 * The format of the GPGSA String is as follows:
 *
 *  $GPGSA,A,3,04,05,,09,12,,,24,,,,,2.5,1.3,2.1*39
 *  |||||||||||||||||||||||||||||||||||||||||||||||
 *  01234567890123456789012345678901234567890123456
 *  |         |         |         |         |
 *  0         10        20        30        40
 *
 *  GPGSA       -   Information about satellite status
 *  A           -   Fix mode, (A)utomatic or (M)anual
 *  3           -   Fix type:
 *                      1 = Invalid
 *                      2 = 2D
 *                      3 = 3D (4 or more satellites)
 *  04,05,...   -   Satellites used in the solution (up to 12)
 *  2.5         -   DOP (dillution of precision)
 *  1.3         -   Horizontal DOP
 *  2.1         -   Vertical DOP
 *  *39         -   Checksum
 *
 * @param char  The full NMEA GPGSA string, from $ to checksum
 */

void QGPSDevice::parseGSA(const char *gsaString)
{
    int j, commaPos;
    char tempString[32];
    char tempChar;
    int tempInt;

    tempString[0] = *(gsaString + 7);
    tempString[1] = '\0';

    sscanf(tempString, "%c", &tempChar);

    if(tempChar == 'A')
        setFixMode(FixAuto);
    else
        setFixMode(FixManual);

    tempString[0] = *(gsaString + 9);
    tempString[1] = '\0';

    sscanf(tempString, "%d", &tempInt);

    if(tempInt == 1)
        setFixType(FixInvalid);
    else if(tempInt == 2)
        setFixType(Fix2D);
    else
        setFixType(Fix3D);

    commaPos = 10;

    for(int index = 0; index < 12; index ++)
    {
        j = commaPos + 1;

        if(*(gsaString + j) != 0x2c && *(gsaString + j) != '*')
        {
            tempString[0] = *(gsaString + j);
            j ++;
            tempString[1] = *(gsaString + j);
            j ++;

            tempString[2] = '\0';
            sscanf(tempString, "%d", &activeSats[index]);
        }
        else
        {
            activeSats[index] = 0;
        }

        commaPos = j;
    }
} // parseGSA()

/**
 * QGPSDevice::parseRMC()
 *
 * Parses an RMC string, which contains the recommended minimum fix
 * data, such as latitude, longitude, altitude, speed, track angle,
 * date, and magnetic variation. Saves us some calculating :)
 *
 * The format of the GPRMC string is as follows:
 *
 *  $GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A
 *  ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 *  01234567890123456789012345678901234567890123456789012345678901234567
 *  |         |         |         |         |         |         |
 *  0         10        20        30        40        50        60
 *
 *  GPRMC       -   Recommended minimum fix data
 *  123519      -   Fix taken at 12:35:19 UTC
 *  A           -   Fix status, (A)ctive or (V)oid
 *  4807.038,N  -   Latitude 48 degrees 07.038' N
 *  01131.000,E -   Longitude 11 degrees, 31.000' E
 *  022.4       -   Ground speed in knots
 *  084.4       -   Track angle in degrees (true north)
 *  230394      -   Date: 23 March 1994
 *  003.1,W     -   Magnetic Variation
 *  *6A         -   Checksum
 *
 * @param char  Full RMC string, from $ to checksum
 */
void QGPSDevice::parseRMC(const QString& g_rmc)
{
  QStringList sl = g_rmc.split(QChar(','));

  Q_ASSERT(sl[0] == "GPRMC");

  // [1] => Fix taken at hhmmss.sss UTC
  QString utc = sl[1];
  setUtcHour(utc.left(2).toInt());
  setUtcMinute(utc.mid(2, 2).toInt());
  setUtcSecond(utc.mid(4, 2).toInt());

  // [2] => fix status
  setFixStatus(sl[2] == "A" ? StatusActive : StatusVoid);

  // [3] => Latitude ddmm.mmmm; [4] => N or S
  parseLatitude(sl[3], sl[4]);

  // [5] => Longitude ddmm.mmmm; [6] => E or W
  parseLongitude(sl[5], sl[6]);

  // [7] => Speed over ground in knots
  setSpeed(sl[7].toFloat());

  // [8] => Heading
  setHeading(sl[8].toFloat());

  // [9] => Date
  int dd = sl[9].left(2).toInt();
  int mm = sl[9].mid(2, 2).toInt();
  int yy = sl[9].right(2).toInt() + (QDate::currentDate().year() / 1000) * 1000;
  setDate(QDate(yy, mm, dd));

  // [10], [11] => Magnetic Variation (not supported by CSR)
  setVariation(sl[10].toFloat());
  setVarCardinal(sl[11] == "E" ? CardinalEast : CardinalWest);
}

/**
 * QGPSDevice::parseGSV()
 *
 * Parses a GPGSV string, which contains satellite position and signal
 * strenght information. parseGSV() fills the satArray array with the
 * PRNs, elevations, azimuths, and SNRs of the visible satellites. This
 * array is based on the position of the satellite in the strings, not
 * the order of the PRNs! (see README for info)
 *
 * The format of the GPGSV string is as follows:
 *
 *  $GPGSV,2,1,08,01,40,083,46,02,17,308,41,12,07,344,39,14,22,228,45*75
 *  ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
 *  01234567890123456789012345678901234567890123456789012345678901234567
 *  |         |         |         |         |         |         |
 *  0         10        20        30        40        50        60
 *
 *  GPGSV       -   Satellite status
 *  2           -   Number of GPGSV sentences for full data
 *  1           -   Current sentence (1 of 2, etc)
 *  08          -   Number of satellites in view
 *
 *  01          -   Satellite PRN
 *  40          -   Elevation, degrees
 *  083         -   Azimuth, degrees
 *  46          -   SNR (signal to noise ratio)
 *      (for up to four satellites per sentence)
 *  *75         -   Checksum
 */
bool QGPSDevice::parseGSV(const QString &p_gsv)
{
  QStringList sl = p_gsv.split(QChar(','));

  Q_ASSERT(sl[0] == "GPGSV");
  // number of sentence
  quint8 ns = sl[1].toShort();
  // current sentence
  quint8 cs = sl[2].toShort();
  // [3] satellites in view (not used)

  if (cs == 1) {
    // clear list on first sentence
    m_SV.clear();
  }
  int idx = 4;
  while (idx + 4 < sl.size()) {
    SatelliteView sv(sl[idx].toShort(),     // id
                     sl[idx + 1].toShort(),  // Elevation
                     sl[idx + 2].toShort(),  // Azimuth
                     sl[idx + 3].toShort()); // SNR

    m_SV.append(sv);
    idx += 4;
  }
  return ns == cs;
}

void QGPSDevice::parseLatitude(const QString& p_Latitude, const QString& p_NS)
{
  if (!p_Latitude.isEmpty()) {
    setLatitude(p_Latitude.toFloat());
    if (p_NS == "N") {
      setLatCardinal(CardinalNorth);
    } else if (p_NS == "S") {
      setLatCardinal(CardinalSouth);
    } else {
      Q_ASSERT(0);
    }
  }
}

void QGPSDevice::parseLongitude(const QString& p_Longitude, const QString& p_EW)
{
  if (!p_Longitude.isEmpty()) {
    setLongitude(p_Longitude.toFloat());
    if (p_EW == "E") {
      setLongCardinal(CardinalEast);
    } else if (p_EW == "W") {
      setLongCardinal(CardinalWest);
    } else {
      Q_ASSERT(0);
    }
  }
}
