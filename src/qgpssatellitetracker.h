/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef QGPSSATELLITETRACKER_H
#define QGPSSATELLITETRACKER_H

#include <QWidget>
#include <satelliteview.h>

class QGPSSatelliteTracker : public QWidget
{

  Q_OBJECT

public:
  QGPSSatelliteTracker(QWidget *parent = 0);

  /**
   * private void QGPSSatelliteTracker::paintEvent()
   *
   * Does the actual drawing of the widget. Reads the satArray array to
   * get satellite positions and PRNs. Reads the satActive array to get
   * which satellites are being used.
   *
   * @param QPaintEvent * Ignored by this function
   */
  void paintEvent(QPaintEvent *);

  /**
   * void QGPSSatelliteTracker::setSatInfo()
   *
   * Stores satellite position in the satArray
   */
  void setSatInfo(const QList<SatelliteView>& p_SV);

private:
  /**
   * private void QGPSSatelliteTracker::getCoordsFromPos()
   *
   * Inputs and elevation and azimuth, and translates them to coordinates
   * that QPainter can use. This one took some thought, but I think it's
   * correct. Satellites jump around, maybe a problem?
   *
   * @param int elevation Elevation of the satellite in degrees, 0 - 90
   * @param int azimuth   Azimuth of the satellite in degrees, 0 - 360
   * @param int &x        Pointer to x-coordinate
   * @param int &y        Pointer to y-coordinate
   */
  void getCoordsForSatellite(SatelliteView p_SV, int &x, int &y);

  int satArray[12][4];
  QList<SatelliteView> m_SV;
};

#endif
