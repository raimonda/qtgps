/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ui_qgpsmainwindowui.h"

#include "qgps.h"
#include "qgpsdevice.h"
#include "qgpssatellitetracker.h"
#include "configdialog.h"

#include <QLabel>
#include <QSettings>

QGPS::QGPS(QWidget *parent) : QMainWindow(parent),
  ui(new Ui::QGPSMainWindowUI)
{
  ui->setupUi(this);
  createStatusBars();

  loadSettings();

  gpsDevice = new QGPSDevice(serialPort);

  connect(ui->actStartGps, SIGNAL(triggered()), SLOT(startGps()));
  connect(ui->actStopGps, SIGNAL(triggered()), SLOT(stopGps()));
  connect(ui->actConfigureQgps, SIGNAL(triggered()), SLOT(configuration()));
  connect(ui->actQuit, SIGNAL(triggered()), SLOT(close()));

  connect(gpsDevice, SIGNAL(updatePosition()), SLOT(updateGpsStatus()));
  connect(gpsDevice, SIGNAL(updateSV()), SLOT(updateSatelliteView()));
  NMEAControlWidget* nc = findChild<NMEAControlWidget*>("nmeaControl");
  nc->setGPSDevice(gpsDevice);

  // set up list of SNR progress bars
  m_SNR[0] = ui->ch1SNR; m_SNR[1] = ui->ch2SNR;
  m_SNR[2] = ui->ch3SNR; m_SNR[3] = ui->ch4SNR;
  m_SNR[4] = ui->ch5SNR; m_SNR[5] = ui->ch6SNR;
  m_SNR[6] = ui->ch7SNR; m_SNR[7] = ui->ch8SNR;
  m_SNR[8] = ui->ch9SNR; m_SNR[9] = ui->ch10SNR;
  m_SNR[10] = ui->ch11SNR; m_SNR[11] = ui->ch12SNR;

  // and channel labels
  m_ChId[0] = ui->ch1Label; m_ChId[1] = ui->ch2Label;
  m_ChId[2] = ui->ch3Label; m_ChId[3] = ui->ch4Label;
  m_ChId[4] = ui->ch5Label; m_ChId[5] = ui->ch6Label;
  m_ChId[6] = ui->ch7Label; m_ChId[7] = ui->ch8Label;
  m_ChId[8] = ui->ch9Label; m_ChId[9] = ui->ch10Label;
  m_ChId[10] = ui->ch11Label; m_ChId[11] = ui->ch12Label;
  updateSatelliteView();
}

void QGPS::createStatusBars()
{
  lblFixStatus = new QLabel(tr("Not connected"), this);
  lblFixStatus->setAlignment(Qt::AlignLeft);
  lblFixStatus->setMinimumSize(lblFixStatus->sizeHint());

  lblFixTime = new QLabel(tr(""), this);
  lblFixTime->setAlignment(Qt::AlignRight);
  lblFixTime->setMinimumSize(lblFixTime->sizeHint());

  ui->statusBar->addWidget(lblFixStatus);
  ui->statusBar->addWidget(lblFixTime);
}

void QGPS::startGps()
{
  // enableControls();
  gpsDevice->startDevice();
  lblFixStatus->setText(tr("No fix"));
  lblFixTime->setText(tr("No UTC Time"));
}

void QGPS::stopGps()
{
  gpsDevice->stopDevice();
  // disableControls();
  lblFixStatus->setText(tr("Not connected"));
  lblFixTime->setText(tr(""));
}

void QGPS::configuration()
{
  ConfigDialog dlg;
  dlg.setBaudRate(gpsDevice->baudRate());
  dlg.setSerialPortName(gpsDevice->serialPort());
  if (dlg.exec()) {
    gpsDevice->setSerialPort(dlg.getSerialPortName());
    gpsDevice->setBaudRate(dlg.getBaudRate());
    saveSettings();
    gpsDevice->restartDevice();
  }
}

void QGPS::updateGpsStatus()
{
    QString latCardinal, longCardinal, varCardinal;

    if(gpsDevice->latCardinal() == QGPSDevice::CardinalNorth)
        latCardinal = "N";
    else
        latCardinal = "S";

    if(gpsDevice->longCardinal() == QGPSDevice::CardinalEast)
        longCardinal = "E";
    else
        longCardinal = "W";

    if(gpsDevice->varCardinal() == QGPSDevice::CardinalEast)
        varCardinal = "E";
    else
        varCardinal = "W";

    QString latitude = QString("%1 %2\' %3\" %4")
        .arg(gpsDevice->latDegrees())
        .arg(gpsDevice->latMinutes())
        .arg(gpsDevice->latSeconds())
        .arg(latCardinal);
    ui->txtLatitude->setText(latitude);

    QString longitude = QString("%1 %2\' %3\" %4")
        .arg(gpsDevice->longDegrees())
        .arg(gpsDevice->longMinutes())
        .arg(gpsDevice->longSeconds())
        .arg(longCardinal);
    ui->txtLongitude->setText(longitude);

   ui-> txtAltitude->setText(
        QString("%1 %2")
            .arg(gpsDevice->altitude())
            .arg(tr("Meters")));

    ui->txtSpeed->setText(
        QString("%1 %2")
            .arg(gpsDevice->speed() * 1.852f)
            .arg(tr("km/h")));

    ui->txtHeading->setText(
        QString("%1").arg(gpsDevice->heading()));

    ui->txtNumSats->setText(
        QString("%1").arg(gpsDevice->numSatellites()));

    switch(gpsDevice->fixQuality()) {
    case 0:
      ui->txtFixType->setText("No fix");
      lblFixStatus->setText("No fix");
      break;
    case 1:
      ui->txtFixType->setText("Valid GPS fix");
      lblFixStatus->setText("Valid GPS fix");
      break;
    case 2:
      ui->txtFixType->setText("Valid DGPS fix");
      lblFixStatus->setText("Valid DGPS fix");
      break;
    default:
      ui->txtFixType->setText("Unknown");
      break;
    }

    ui->txtVariation->setText(
        QString("%1 %2")
            .arg(gpsDevice->variation())
            .arg(varCardinal));
    lblFixTime->setText(gpsDevice->utcTime());
}

void QGPS::updateSatelliteView()
{
  const QList<SatelliteView>& svl = gpsDevice->satelliteViews();
  ui->satTracker->setSatInfo(svl);
  int i = 0;
  foreach (const SatelliteView sv, svl) {
    m_SNR[i]->setValue(sv.getSNR());
    m_SNR[i]->setEnabled(true);
    m_ChId[i++]->setText(QString("%1").arg(sv.getID()));
  }
  for (; i < 12; i++) {
    m_SNR[i]->setEnabled(false);
    m_ChId[i]->setText("");
  }
}

void QGPS::loadSettings()
{
    serialPort = "/dev/ttyUSB0";
}

void QGPS::saveSettings()
{
}
