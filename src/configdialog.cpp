#include "configdialog.h"
#include "ui_configdialog.h"

ConfigDialog::ConfigDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ConfigDialog)
{
  ui->setupUi(this);
  ui->speedComboBox->addItems(QStringList("1200") << "2400" << "4800" << "9600");
}

ConfigDialog::~ConfigDialog()
{
  delete ui;
}

void ConfigDialog::setSerialPortName(const QString p_PortName)
{
  ui->portLineEdit->setText(p_PortName);
}

QString ConfigDialog::getSerialPortName() const
{
  return ui->portLineEdit->text();
}

void ConfigDialog::setBaudRate(const quint32 p_BaudRate)
{
  int index = 0;
  switch (p_BaudRate) {
  case 1200:
    index = 0;
    break;
  case 2400:
    index = 1;
    break;
  case 4800:
    index = 2;
    break;
  case 9600:
    index = 3;
    break;
  default:
    Q_ASSERT_X(0, "setBaudRate", "Baud rate not supported");
    break;
  }
  ui->speedComboBox->setCurrentIndex(index);
}

quint32 ConfigDialog::getBaudRate() const
{
  switch (ui->speedComboBox->currentIndex()) {
  default:
  case 0:
    return 1200;
  case 1:
    return 2400;
  case 2:
    return 4800;
  case 3:
    return 9600;
  }
}
