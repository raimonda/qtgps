#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>

namespace Ui {
class ConfigDialog;
}

class ConfigDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ConfigDialog(QWidget *parent = 0);
  ~ConfigDialog();

  void setSerialPortName(const QString p_PortName);
  QString getSerialPortName() const;

  void setBaudRate(const quint32 p_BaudRate);
  quint32 getBaudRate() const;
  //QString get

private:
  Ui::ConfigDialog *ui;
};

#endif // CONFIGDIALOG_H
