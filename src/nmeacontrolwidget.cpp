#include "nmeacontrolwidget.h"
#include "ui_nmeacontrolwidget.h"

#include "qgpsdevice.h"

NMEAControlWidget::NMEAControlWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::NMEAControlWidget), m_GPSDevice(0)
{
  ui->setupUi(this);
  ui->msgComboBox->addItems(QStringList("GGA") << "GLL" << "GSA" << "GSV" << "RMC" << "VTG");
  ui->speedComboBox->addItems(QStringList("1200") << "2400" << "4800" << "9600");
}

NMEAControlWidget::~NMEAControlWidget()
{
  delete ui;
}

void NMEAControlWidget::setGPSDevice(QGPSDevice *p_Dev)
{
  // disconnect from old device
  disconnect(m_GPSDevice);
  m_GPSDevice = p_Dev;
  if (m_GPSDevice) {
   connect(m_GPSDevice, SIGNAL(NMEAMessageReceived(QString)), ui->plainTextEdit, SLOT(appendPlainText(QString)));
  }
}

void NMEAControlWidget::on_queryComposePushButton_clicked()
{
  quint8 msg = ui->msgComboBox->currentIndex();
  quint8 mode = 0;
  quint8 rate = ui->rateSpinBox->value();
  quint8 chkOn = 1;

  ui->msgLineEdit->setText(QString("PSRF103,%1,%2,%3,%4")
                            .arg(msg, 2, 16, QChar('0'))
                            .arg(mode, 2, 16, QChar('0'))
                            .arg(rate, 2, 16, QChar('0'))
                            .arg(chkOn, 2, 16, QChar('0')));
}

void NMEAControlWidget::on_sendNmeaButton_clicked()
{
  if (m_GPSDevice && m_GPSDevice->isRunning()) {
    m_GPSDevice->sendNMEAMessage(ui->msgLineEdit->text().toAscii());
  }
}

void NMEAControlWidget::on_msgLineEdit_textChanged(const QString& text)
{
  QString upperText = text.toUpper();
  ui->chksmLabel->setText("*" + QGPSDevice::nmeaChecksum(upperText));
  ui->msgLineEdit->setText(upperText);
}

void NMEAControlWidget::on_serialPortComposeButton_clicked()
{
  quint16 speed;
  switch (ui->speedComboBox->currentIndex()) {
  case 0:
    speed = 1200;
    break;
  case 1:
    speed = 2400;
    break;
  case 2:
    speed = 4800;
    break;
  default:
  case 3:
    speed = 9600;
    break;
  }

  ui->msgLineEdit->setText(QString("PSRF100,1,%1,8,1,0")
                          .arg(speed));
}
