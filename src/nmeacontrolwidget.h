#ifndef NMEACONTROLWIDGET_H
#define NMEACONTROLWIDGET_H

#include <QWidget>

namespace Ui {
class NMEAControlWidget;
}

class QGPSDevice;

class NMEAControlWidget : public QWidget
{
  Q_OBJECT

public:
  explicit NMEAControlWidget(QWidget *parent = 0);
  ~NMEAControlWidget();

  void setGPSDevice(QGPSDevice* p_Dev);

protected slots:
  void on_msgLineEdit_textChanged(const QString &text);
  void on_sendNmeaButton_clicked();
  void on_queryComposePushButton_clicked();
  void on_serialPortComposeButton_clicked();

private:
  Ui::NMEAControlWidget *ui;
  QGPSDevice* m_GPSDevice;
};

#endif // NMEACONTROLWIDGET_H
