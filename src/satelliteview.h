#ifndef SATELLITEVIEW_H
#define SATELLITEVIEW_H

#include <QtCore>

class SatelliteView
{
public:
  SatelliteView(quint8 p_ID = 0, quint8 p_Elevation = 0,
                quint8 p_Azimuth = 0, quint8 p_SNR = 0) :
    m_ID(p_ID), m_Elevation(p_Elevation),
    m_Azimuth(p_Azimuth), m_SNR(p_SNR)
  {}

  void setID(quint8 p_ID) { m_ID =  p_ID; }
  void setElevation(quint8 p_Elevation) { m_Elevation =  p_Elevation; }
  void setAzimuth(quint8 p_Azimuth) { m_Azimuth =  p_Azimuth; }
  void setSNR(quint8 p_SNR) { m_SNR =  p_SNR; }

  quint8 getID() const { return m_ID; }
  quint8 getElevation() const { return m_Elevation; }
  quint8 getAzimuth() const { return m_Azimuth; }
  quint8 getSNR() const { return m_SNR; }

private:
  quint8 m_ID;
  quint8 m_Elevation;
  quint8 m_Azimuth;
  quint8 m_SNR;
};

#endif // SATELLITEVIEW_H
