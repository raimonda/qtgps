/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "qgpssatellitetracker.h"
#include <QtGui>
#include <QPaintEvent>

#include <math.h>

QGPSSatelliteTracker::QGPSSatelliteTracker(QWidget *parent) : QWidget(parent)
{
    for(int i = 0; i < 12; i ++)
    {
        satArray[i][0] = satArray[i][1] = satArray[i][2] = satArray[i][3] = 0;
    }
}

void QGPSSatelliteTracker::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setPen(QPen(Qt::darkGray));
    painter.setBrush(QBrush(Qt::darkGray, Qt::NoBrush));

    // first paint the two reference circles, one at 0 degrees, and
    // the other at 45 degrees
    int x = rect().x();
    painter.drawEllipse(x, rect().y(), rect().width() - 1, rect().height() - 1);
    painter.drawEllipse(
        rect().x() + (rect().width() / 4),
        rect().y() + (rect().height() / 4),
        rect().width() / 2,
        rect().height() / 2);

    // now the reference lines, one vertical and the other horizontal
    painter.drawLine(rect().x(), rect().height() / 2, rect().width(), rect().height() / 2);
    painter.drawLine(rect().width() / 2, rect().y(), rect().width() / 2, rect().height());

    // now plot the satellites
    painter.setPen(QPen(Qt::red));
    painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));

    foreach (SatelliteView sv, m_SV) {
      int x, y;

      getCoordsForSatellite(sv, x, y);
      painter.drawEllipse(x - 2, y - 2, 4, 4);
      painter.drawText(x + 5, y - 1, QString("%1").arg(sv.getID()));
    }
}

void QGPSSatelliteTracker::setSatInfo(const QList<SatelliteView>& p_SV)
{
  m_SV = p_SV;
  update();
}

void QGPSSatelliteTracker::getCoordsForSatellite(SatelliteView p_SV, int &x, int &y)
{
    int theta;
    int elevation = p_SV.getElevation();
    int azimuth = p_SV.getAzimuth();

    // translate the azimuth angle to an angle relative to the x-axis
    // i.e., "snap" it to the x-axis
    if(azimuth > 270)
        theta = azimuth - 270;
    else if(azimuth > 180)
        theta = 270 - azimuth;
    else if(azimuth > 90)
        theta = azimuth - 90;
    else
        theta = 90 - azimuth;

    // since the "origin" of the sky is 90 (directly overhead), reverse
    // elevation to make the origin 0 (an elevation of 90 becomes 0, etc)
    elevation = 90 - elevation;

    // you should know this (slept too much in trig)
    x = (cos(theta)) * elevation;
    y = (sin(theta)) * elevation;

    // here we need to make sure that coordinates in quadrants II, III, and IV
    // have the proper signs
    if(azimuth > 270) {
        x = x - (2 * x);
    }
    else if(azimuth > 180) {
        x = x - (2 * x);
        y = y - (2 * y);
    }
    else if(azimuth > 90) {
        y = y - (2 * y);
    }

    // scale our x and y to the size of the widget
    x = (rect().width() / 180) * x;
    y = (rect().height() / 180) * y;

    // translate our coordinates by shifting the plane right and up
    // to match the coordinate plane of the widget (to prevent drawing
    // only one quarter of the plane onto the widget
    x = (rect().width() / 2) + x;
    y = (rect().height() / 2) + y;

    // mirror y since QPainter's positive-y goes down and the trigonometric
    // positive-y goes up
    y = 200 - y;
}
