/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QObject>
#include <satelliteview.h>

class QextSerialPort;

class QGPSDevice : public QObject
{
  Q_OBJECT

public:

  QGPSDevice(const QString serial_port, const quint32 p_BaudRate = 9600);

  static QString nmeaChecksum(const QByteArray &p_Msg);
  static QString nmeaChecksum(const QString& p_Msg);

  enum FixMode
  {
      FixAuto,
      FixManual
  };

  enum FixStatus
  {
      StatusActive,
      StatusVoid
  };

  enum FixType
  {
      FixInvalid,
      Fix2D,
      Fix3D
  };

  enum CardinalDirection
  {
      CardinalNorth,
      CardinalSouth,
      CardinalEast,
      CardinalWest,
      CardinalNone
  };

  void setSerialPort(QString new_serialPort) { m_SerialPort = new_serialPort; }
  void setBaudRate(quint32 p_BaudRate) { m_BaudRate = p_BaudRate; }

  QString serialPort() const   { return m_SerialPort;    }
  quint32 baudRate() const     { return m_BaudRate;      }

  QDate  date() const           { return cur_date;          }
  QString utcTime() const;

  int   utcHour() const        { return cur_utcHour;       }
  int   utcMinute() const      { return cur_utcMinute;     }
  int   utcSecond() const      { return cur_utcSecond;     }
  int   fixQuality() const     { return cur_fixQuality;    }
  int   numSatellites() const  { return cur_numSatellites; }

  float latitude() const       { return cur_latitude;      }
  float longitude() const      { return cur_longitude;     }
  float altitude() const       { return cur_altitude;      }
  float heading() const        { return cur_heading;       }
  float speed() const          { return cur_speed;         }
  float variation() const      { return cur_variation;     }
  float dillution() const      { return cur_dillution;     }

  CardinalDirection latCardinal() const    { return cur_latCardinal;   }
  CardinalDirection longCardinal() const   { return cur_longCardinal;  }
  CardinalDirection varCardinal() const    { return cur_varCardinal;   }

  const QList<SatelliteView>& satelliteViews() const { return m_SV; }

  // some convinience functions

  int latDegrees() const;
  int latMinutes() const;
  int latSeconds() const;

  int longDegrees() const;
  int longMinutes() const;
  int longSeconds() const;

public slots:
  void startDevice();

  /**
   * QGPSDevice::stopDevice()
   *
   * Stop device by closing serial port
   */
  void stopDevice();

  /**
   * QGPSDevice::restartDevice()
   *
   * Stop and restart device if it is currently running
   */
  void restartDevice();
  bool isRunning() const;

  void sendNMEAMessage(QByteArray p_Msg);

signals:
  void updatePosition();
  void updateSV();
  void NMEAMessageReceived(const QString);

protected slots:
  void onDataAvailable();

  void parseMessage(const QString p_Message);

private:
  typedef enum {
    eSerIdle,
    eSerCRLF,
  } eNMEAState_t;

  QextSerialPort* m_Port;

  // functions to set various properties - private

  void setDate(QDate new_date)                            { cur_date          = new_date;         }
  void setUtcHour(int new_utcHour)                        { cur_utcHour       = new_utcHour;      }
  void setUtcMinute(int new_utcMinute)                    { cur_utcMinute     = new_utcMinute;    }
  void setUtcSecond(int new_utcSecond)                    { cur_utcSecond     = new_utcSecond;    }
  void setLatitude(float new_latitude)                    { cur_latitude      = new_latitude;     }
  void setLongitude(float new_longitude)                  { cur_longitude     = new_longitude;    }
  void setAltitude(float new_altitude)                    { cur_altitude      = new_altitude;     }
  void setHeading(float new_heading)                      { cur_heading       = new_heading;      }
  void setSpeed(float new_speed)                          { cur_speed         = new_speed;        }
  void setVariation(float new_variation)                  { cur_variation     = new_variation;    }
  void setLatCardinal(CardinalDirection new_direction)    { cur_latCardinal   = new_direction;    }
  void setLongCardinal(CardinalDirection new_direction)   { cur_longCardinal  = new_direction;    }
  void setVarCardinal(CardinalDirection new_direction)    { cur_varCardinal   = new_direction;    }
  void setFixQuality(int new_fixQuality)                  { cur_fixQuality    = new_fixQuality;   }
  void setDillution(float new_dillution)                  { cur_dillution     = new_dillution;    }
  void setNumSatellites(int new_numSatellites)            { cur_numSatellites = new_numSatellites;}
  void setFixType(FixType new_fixType)                    { cur_fixType       = new_fixType;      }
  void setFixMode(FixMode new_fixMode)                    { cur_fixMode       = new_fixMode;      }
  void setFixStatus(FixStatus new_fixStatus)              { cur_fixStatus     = new_fixStatus;    }

  QString m_SerialPort;
  quint32 m_BaudRate;

  QDate cur_date;

  unsigned int cur_utcHour;
  unsigned int cur_utcMinute;
  unsigned int cur_utcSecond;

  float cur_latitude;
  float cur_longitude;
  float cur_altitude;
  float cur_heading;
  float cur_speed;
  float cur_variation;
  float cur_dillution;

  CardinalDirection cur_latCardinal;
  CardinalDirection cur_longCardinal;
  CardinalDirection cur_varCardinal;

  unsigned int cur_fixQuality;
  unsigned int cur_numSatellites;

  QList<SatelliteView> m_SV;
  unsigned int satArray[12][4];
  unsigned int activeSats[12];

  FixType cur_fixType;
  FixMode cur_fixMode;
  FixStatus cur_fixStatus;

  bool openDevice();
  void closeDevice();

  void parseGGA(const QString& p_gga);
  void parseGSA(const char *gsaString = 0);
  bool parseGSV(const QString& p_gsv);
  void parseRMC(const QString& p_rmc);
  void parseLatitude(const QString &p_Latitude, const QString &p_NS);
  void parseLongitude(const QString& p_Longitude, const QString& p_EW);
};

