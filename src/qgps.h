/***************************************************************************
 *   Copyright (C) 2005 by Robin Gingras                                   *
 *   neozenkai@cox.net                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef QGPS_H
#define QGPS_H

#include <QLabel>
#include <QMainWindow>

namespace Ui {
class QGPSMainWindowUI;
}

class QGPSDevice;
class QLabel;
class QProgressBar;
class QString;

class QGPS : public QMainWindow
{
  Q_OBJECT

public:
  QGPS(QWidget *parent = 0);

protected slots:
  void configuration();

private:

  QGPSDevice  *gpsDevice;

  QString     serialPort;

  void createStatusBars();

  void loadSettings();
  void saveSettings();

  QLabel *lblFixStatus;
  QLabel *lblFixTime;

private slots:

  void updateGpsStatus();
  void updateSatelliteView();
  void startGps();
  void stopGps();

private:
  Ui::QGPSMainWindowUI *ui;
  QProgressBar* m_SNR[12];
  QLabel* m_ChId[12];
};

#endif
